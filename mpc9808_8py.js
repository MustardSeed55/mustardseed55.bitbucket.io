var mpc9808_8py =
[
    [ "MPC9808_I2C", "classmpc9808_1_1MPC9808__I2C.html", "classmpc9808_1_1MPC9808__I2C" ],
    [ "Manufacturer_ID", "mpc9808_8py.html#abaf9c24b146543100faa6d1b3849e69e", null ],
    [ "Manufacturer_Memaddr", "mpc9808_8py.html#a0ddc65ded48f114dee47f69934f2811a", null ],
    [ "Read_Memaddr", "mpc9808_8py.html#afece5a2afda203a6e8eba0c5e14f013a", null ],
    [ "Slave_Addr", "mpc9808_8py.html#aab407de153b792799ad30f2d0a40a95e", null ],
    [ "task1", "mpc9808_8py.html#a0565792442123dc73e92ba0d8c4e23c3", null ]
];