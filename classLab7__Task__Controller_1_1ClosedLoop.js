var classLab7__Task__Controller_1_1ClosedLoop =
[
    [ "__init__", "classLab7__Task__Controller_1_1ClosedLoop.html#aaeec6f33bb161f02080a106b9015e090", null ],
    [ "get_Kp", "classLab7__Task__Controller_1_1ClosedLoop.html#a84bd5ac941b8a447c4718a00e31f1cd5", null ],
    [ "run", "classLab7__Task__Controller_1_1ClosedLoop.html#a7e8db2e540cb2c8c3492e385baae6241", null ],
    [ "set_Kp", "classLab7__Task__Controller_1_1ClosedLoop.html#a553ee7870e5e662cdcf41c844549567e", null ],
    [ "delta_t", "classLab7__Task__Controller_1_1ClosedLoop.html#ae421aa972b8f8e63b83d459ca5475765", null ],
    [ "Kp", "classLab7__Task__Controller_1_1ClosedLoop.html#a0d8dd1f5b2eca19336fecfb0146a0f51", null ],
    [ "V_DC", "classLab7__Task__Controller_1_1ClosedLoop.html#ab0960102a51e9a8ff38377ac52cb939a", null ]
];