var Lab7__main_8py =
[
    [ "counter_size", "Lab7__main_8py.html#aabe07b2ca1a56daa3edc59df3c58319b", null ],
    [ "dbg", "Lab7__main_8py.html#aa37c6d5a6343fa6955eed044186b320a", null ],
    [ "IN1_ch", "Lab7__main_8py.html#abfe1732b96f2eae32bc6b1577bb47857", null ],
    [ "IN1_pin", "Lab7__main_8py.html#ad4b3510f3c094622488a3f058a649288", null ],
    [ "IN2_ch", "Lab7__main_8py.html#adff9f1a167b8d298c3d2b9fde236ee3b", null ],
    [ "IN2_pin", "Lab7__main_8py.html#a3b41cb4c71c0f5d789b7988b03cf8e78", null ],
    [ "nSLEEP_pin", "Lab7__main_8py.html#a077d0fab02846dc69c7e71ec736ffc1f", null ],
    [ "pin1", "Lab7__main_8py.html#aed7ec98a6d473385944ddba0eac29c80", null ],
    [ "pin1_ch", "Lab7__main_8py.html#a0f74c4b88490838ff4d613566248c8bd", null ],
    [ "pin2", "Lab7__main_8py.html#a9e524d9fd08e86f4f85b3d341b78b3e6", null ],
    [ "pin2_ch", "Lab7__main_8py.html#a86aa10e86ab2f8ee47ebd37d3d222d7a", null ],
    [ "PPR", "Lab7__main_8py.html#a37e84e60b86933aa7d7da3cf4dbdf2e7", null ],
    [ "rpm", "Lab7__main_8py.html#a7db089569d6042df31a6bfd5dcffba90", null ],
    [ "task1", "Lab7__main_8py.html#a09424634183c46e9b56957adcb215807", null ],
    [ "task2", "Lab7__main_8py.html#a1e4c5e0c692ec19349992e48b257fea8", null ],
    [ "task3", "Lab7__main_8py.html#aa0a0b8ea88280bad27b63013e17cb79e", null ],
    [ "timer_ENC", "Lab7__main_8py.html#af25f5ca73d4420a708b695bb4b8c2689", null ],
    [ "timer_MOT", "Lab7__main_8py.html#a43e4f1922ea07e76c8473d08fb2b5a2e", null ]
];