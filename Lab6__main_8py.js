var Lab6__main_8py =
[
    [ "counter_size", "Lab6__main_8py.html#a38359db74b72af8f065d91b4f6463037", null ],
    [ "dbg", "Lab6__main_8py.html#a31262c64f2d2132e5e254a31b8af86f4", null ],
    [ "IN1_ch", "Lab6__main_8py.html#aaf0c04b382c2f6089721373fc1624970", null ],
    [ "IN1_pin", "Lab6__main_8py.html#ab49d56d99228775e908fb25c773c1499", null ],
    [ "IN2_ch", "Lab6__main_8py.html#a9cfd820c629ba3e2b54feb3ef9b7de40", null ],
    [ "IN2_pin", "Lab6__main_8py.html#a56b9c5130b30193562b2b5368c0bcf2b", null ],
    [ "nSLEEP_pin", "Lab6__main_8py.html#a948bbdb9da3261f7f093bf1f3c6816e1", null ],
    [ "pin1", "Lab6__main_8py.html#a544858947c29f783df2d52729d2e5fe3", null ],
    [ "pin1_ch", "Lab6__main_8py.html#a1742359f10607067a7379413c1713195", null ],
    [ "pin2", "Lab6__main_8py.html#a0651a4b4f6d0b7b0f4e35d5e8e1a5c63", null ],
    [ "pin2_ch", "Lab6__main_8py.html#aaf6a8f8d03f9af6d09ceb8218e9d1303", null ],
    [ "PPR", "Lab6__main_8py.html#abb97ecdebdc4b812d7ccc7c8ba39522f", null ],
    [ "rpm", "Lab6__main_8py.html#ab7288f39b956d521eecc23c661f32ce0", null ],
    [ "task1", "Lab6__main_8py.html#ac8eee2143095cf98365143242b3cf89d", null ],
    [ "task2", "Lab6__main_8py.html#acdc203d9bb9e85c1ace72ff0460205dd", null ],
    [ "task3", "Lab6__main_8py.html#abff7671574d6472a191a0040548673bb", null ],
    [ "timer_ENC", "Lab6__main_8py.html#aabbecadb66fe77ef2f1ff7d7b4c4d76d", null ],
    [ "timer_MOT", "Lab6__main_8py.html#a0666365c0d0d6061558468b89ec9f61a", null ]
];