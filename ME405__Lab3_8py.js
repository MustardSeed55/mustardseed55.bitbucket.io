var ME405__Lab3_8py =
[
    [ "ADC_count", "ME405__Lab3_8py.html#aca10ca39527e0e8434b414119accb621", null ],
    [ "data_CSV", "ME405__Lab3_8py.html#ad621f9854f8553f7f5ab76d163f96da5", null ],
    [ "data_list", "ME405__Lab3_8py.html#a7f3dd1ec9fa121e67db988b62ea3f40b", null ],
    [ "data_string", "ME405__Lab3_8py.html#a82f4f80d68a452fc9a1b9777ea38d3f2", null ],
    [ "data_tup", "ME405__Lab3_8py.html#a03027d0509f266bbdd935c111de15736", null ],
    [ "delimiter", "ME405__Lab3_8py.html#abc59a21bacacc846feb9e2d4446d8ddf", null ],
    [ "fmt", "ME405__Lab3_8py.html#a81390565edf6c0c8110fd721c608f6b7", null ],
    [ "markersize", "ME405__Lab3_8py.html#aa40a35fc7b9b3f823ab1253a25f0af8e", null ],
    [ "ser", "ME405__Lab3_8py.html#a82c1d340f87e082cb03482966b9d3433", null ],
    [ "start_index", "ME405__Lab3_8py.html#a5126eebc5d8bf247ae5c01862b2cbdfb", null ],
    [ "step_function", "ME405__Lab3_8py.html#a4771870b96f27c9130d64817de45502e", null ],
    [ "time", "ME405__Lab3_8py.html#a9e08cad259aea9c93dee12bdb8ab1679", null ],
    [ "user_input", "ME405__Lab3_8py.html#a3e9a193a15a1ea9edab3ff49bb836f86", null ]
];