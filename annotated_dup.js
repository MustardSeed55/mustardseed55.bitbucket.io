var annotated_dup =
[
    [ "bno055", null, [
      [ "BNO055", "classbno055_1_1BNO055.html", "classbno055_1_1BNO055" ]
    ] ],
    [ "bno055_base", null, [
      [ "BNO055_BASE", "classbno055__base_1_1BNO055__BASE.html", "classbno055__base_1_1BNO055__BASE" ]
    ] ],
    [ "Elevator", null, [
      [ "Button", "classElevator_1_1Button.html", "classElevator_1_1Button" ],
      [ "Elevator", "classElevator_1_1Elevator.html", "classElevator_1_1Elevator" ],
      [ "MotorDriver", "classElevator_1_1MotorDriver.html", "classElevator_1_1MotorDriver" ]
    ] ],
    [ "lab1", null, [
      [ "Fibonnaci", "classlab1_1_1Fibonnaci.html", "classlab1_1_1Fibonnaci" ]
    ] ],
    [ "Lab2", null, [
      [ "LED_Controller", "classLab2_1_1LED__Controller.html", "classLab2_1_1LED__Controller" ]
    ] ],
    [ "Lab5_BLE", null, [
      [ "BLE_Driver", "classLab5__BLE_1_1BLE__Driver.html", "classLab5__BLE_1_1BLE__Driver" ],
      [ "BLE_FSM", "classLab5__BLE_1_1BLE__FSM.html", "classLab5__BLE_1_1BLE__FSM" ]
    ] ],
    [ "Lab7_Task_Controller", null, [
      [ "ClosedLoop", "classLab7__Task__Controller_1_1ClosedLoop.html", "classLab7__Task__Controller_1_1ClosedLoop" ],
      [ "Controller", "classLab7__Task__Controller_1_1Controller.html", "classLab7__Task__Controller_1_1Controller" ],
      [ "Encoder", "classLab7__Task__Controller_1_1Encoder.html", "classLab7__Task__Controller_1_1Encoder" ],
      [ "Motor", "classLab7__Task__Controller_1_1Motor.html", "classLab7__Task__Controller_1_1Motor" ]
    ] ],
    [ "ME405_Drivers", null, [
      [ "Encoder", "classME405__Drivers_1_1Encoder.html", "classME405__Drivers_1_1Encoder" ],
      [ "Motor", "classME405__Drivers_1_1Motor.html", "classME405__Drivers_1_1Motor" ]
    ] ],
    [ "ME405_Lab7", null, [
      [ "TouchScreen", "classME405__Lab7_1_1TouchScreen.html", "classME405__Lab7_1_1TouchScreen" ]
    ] ],
    [ "ME405_Lab8", null, [
      [ "Encoder", "classME405__Lab8_1_1Encoder.html", "classME405__Lab8_1_1Encoder" ],
      [ "Motor", "classME405__Lab8_1_1Motor.html", "classME405__Lab8_1_1Motor" ]
    ] ],
    [ "ME405_Touchscreen", null, [
      [ "TouchScreen", "classME405__Touchscreen_1_1TouchScreen.html", "classME405__Touchscreen_1_1TouchScreen" ]
    ] ],
    [ "mpc9808", null, [
      [ "MPC9808_I2C", "classmpc9808_1_1MPC9808__I2C.html", "classmpc9808_1_1MPC9808__I2C" ]
    ] ],
    [ "Task_Controller", null, [
      [ "ClosedLoop", "classTask__Controller_1_1ClosedLoop.html", "classTask__Controller_1_1ClosedLoop" ],
      [ "Controller", "classTask__Controller_1_1Controller.html", "classTask__Controller_1_1Controller" ],
      [ "Encoder", "classTask__Controller_1_1Encoder.html", "classTask__Controller_1_1Encoder" ],
      [ "Motor", "classTask__Controller_1_1Motor.html", "classTask__Controller_1_1Motor" ]
    ] ],
    [ "Task_Data", null, [
      [ "Data", "classTask__Data_1_1Data.html", "classTask__Data_1_1Data" ],
      [ "Encoder", "classTask__Data_1_1Encoder.html", "classTask__Data_1_1Encoder" ]
    ] ],
    [ "Task_Encoder", null, [
      [ "Encoder", "classTask__Encoder_1_1Encoder.html", "classTask__Encoder_1_1Encoder" ]
    ] ],
    [ "Task_UI", null, [
      [ "UI", "classTask__UI_1_1UI.html", "classTask__UI_1_1UI" ]
    ] ]
];