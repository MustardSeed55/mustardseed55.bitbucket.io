var dir_7ba879b5937690b788199beb6e9327f6 =
[
    [ "Lecture Homework", "dir_dce71e6dcf122767279554b6d4e4b0bb.html", "dir_dce71e6dcf122767279554b6d4e4b0bb" ],
    [ "ME405_Drivers.py", "ME405__Drivers_8py.html", [
      [ "Encoder", "classME405__Drivers_1_1Encoder.html", "classME405__Drivers_1_1Encoder" ],
      [ "Motor", "classME405__Drivers_1_1Motor.html", "classME405__Drivers_1_1Motor" ]
    ] ],
    [ "ME405_Lab1.py", "ME405__Lab1_8py.html", "ME405__Lab1_8py" ],
    [ "ME405_Lab2.py", "ME405__Lab2_8py.html", "ME405__Lab2_8py" ],
    [ "ME405_Lab3.py", "ME405__Lab3_8py.html", "ME405__Lab3_8py" ],
    [ "ME405_Lab3_main.py", "ME405__Lab3__main_8py.html", "ME405__Lab3__main_8py" ],
    [ "ME405_Lab4_main.py", "ME405__Lab4__main_8py.html", "ME405__Lab4__main_8py" ],
    [ "ME405_Lab4_Plot.py", "ME405__Lab4__Plot_8py.html", "ME405__Lab4__Plot_8py" ],
    [ "ME405_Lab7.py", "ME405__Lab7_8py.html", [
      [ "TouchScreen", "classME405__Lab7_1_1TouchScreen.html", "classME405__Lab7_1_1TouchScreen" ]
    ] ],
    [ "ME405_Lab7_main.py", "ME405__Lab7__main_8py.html", "ME405__Lab7__main_8py" ],
    [ "ME405_Lab8.py", "ME405__Lab8_8py.html", [
      [ "Encoder", "classME405__Lab8_1_1Encoder.html", "classME405__Lab8_1_1Encoder" ],
      [ "Motor", "classME405__Lab8_1_1Motor.html", "classME405__Lab8_1_1Motor" ]
    ] ],
    [ "ME405_Lab8_main.py", "ME405__Lab8__main_8py.html", "ME405__Lab8__main_8py" ],
    [ "ME405_main.py", "ME405__main_8py.html", "ME405__main_8py" ],
    [ "ME405_Touchscreen.py", "ME405__Touchscreen_8py.html", [
      [ "TouchScreen", "classME405__Touchscreen_1_1TouchScreen.html", "classME405__Touchscreen_1_1TouchScreen" ]
    ] ],
    [ "mpc9808.py", "mpc9808_8py.html", "mpc9808_8py" ]
];