var page_ME405 =
[
    [ "Lab 1: Vending Machine", "subpage_ME405_lab1.html", null ],
    [ "Lab 2: Reaction Time", "subpage_ME405_lab2.html", null ],
    [ "Lab 3: USER Button Response", "subpage_ME405_lab3.html", null ],
    [ "Lab 4: Temperature and I2C Communication", "subpage_ME405_lab4.html", null ],
    [ "Lab 5: Rotating Platform Dynamics", "subpage_ME405_lab5.html", null ],
    [ "Lab 6: Platform Dynamics Simulations", "subpage_ME405_lab6.html", null ],
    [ "Lab 7: Platform Touchscreen", "subpage_ME405_lab7.html", null ],
    [ "Lab 8: Motor & Encoder Drivers", "subpage_ME405_lab8.html", null ],
    [ "Lab 9: Balance Ball on Platform", "subpage_ME405_lab9.html", null ]
];