var classTask__Controller_1_1ClosedLoop =
[
    [ "__init__", "classTask__Controller_1_1ClosedLoop.html#ac5f90d314015268dacfe7fed79e768b8", null ],
    [ "get_Kp", "classTask__Controller_1_1ClosedLoop.html#ac3a3472798d86271caab7133f02d9776", null ],
    [ "run", "classTask__Controller_1_1ClosedLoop.html#a2027f431d33957294210eb9e1ff1a3b1", null ],
    [ "set_Kp", "classTask__Controller_1_1ClosedLoop.html#aa0bf0da73d8f262b26cf00fab08a248c", null ],
    [ "delta_t", "classTask__Controller_1_1ClosedLoop.html#a3b78479e23fd330771fa7828ec63453e", null ],
    [ "Kp", "classTask__Controller_1_1ClosedLoop.html#ac41e9384c70f5e2d0b34a9c23b44406a", null ],
    [ "Omega", "classTask__Controller_1_1ClosedLoop.html#a5c6d517b29ab274b29ab686e450838cc", null ],
    [ "Omega_ref", "classTask__Controller_1_1ClosedLoop.html#a35465578e937e4ed23b8dcfb3abe5773", null ],
    [ "V_DC", "classTask__Controller_1_1ClosedLoop.html#a5c595bc18860963764a38e65cb738f9b", null ]
];