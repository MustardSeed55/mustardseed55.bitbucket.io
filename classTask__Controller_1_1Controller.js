var classTask__Controller_1_1Controller =
[
    [ "__init__", "classTask__Controller_1_1Controller.html#aaf7cd43d8ae8ebe98cc3e2eb48ce9b35", null ],
    [ "run", "classTask__Controller_1_1Controller.html#ac60fa40df3b146b4af0f862b46c5e2c6", null ],
    [ "transitionTo", "classTask__Controller_1_1Controller.html#aed2faf851203bae69b90124eedd2872e", null ],
    [ "ClosedLoop", "classTask__Controller_1_1Controller.html#ac7f5de1a51859ea98c1a7436f56793f2", null ],
    [ "current_time", "classTask__Controller_1_1Controller.html#a2f7c49d6849e5b44b03f8afd9c1dbc9e", null ],
    [ "dbg", "classTask__Controller_1_1Controller.html#a3d9d4d26bc233425a9194b6a2e6ec584", null ],
    [ "Encoder", "classTask__Controller_1_1Controller.html#a92c32bdad9caf22b3f121ae0065fe431", null ],
    [ "Motor", "classTask__Controller_1_1Controller.html#af9c8696f6c8e92b3c25976fdbe63a67f", null ],
    [ "next_time", "classTask__Controller_1_1Controller.html#ad37503a0fad22aeee3a0ace95ceab7cc", null ],
    [ "Omega", "classTask__Controller_1_1Controller.html#a83b6f2c75d9c4e919bb42970195b2601", null ],
    [ "Omega_ref", "classTask__Controller_1_1Controller.html#abe87d10096a74cfaefe7d81f67e1e483", null ],
    [ "period", "classTask__Controller_1_1Controller.html#a135960bf76d8e78b31639d5d30ca066f", null ],
    [ "run_time", "classTask__Controller_1_1Controller.html#a2b4b77e4732dd6753857657cba00011c", null ],
    [ "runs", "classTask__Controller_1_1Controller.html#ac81c77d856416b57d834f03f90cc9038", null ],
    [ "ser", "classTask__Controller_1_1Controller.html#a3cebe142de91e9057868907263646d1c", null ],
    [ "start_time", "classTask__Controller_1_1Controller.html#a1121a3ade23cd4b84797c5e43a312579", null ],
    [ "state", "classTask__Controller_1_1Controller.html#afa08f06ab751fb04df3211d5a259ff37", null ],
    [ "time", "classTask__Controller_1_1Controller.html#a7c8333c7260802fb706d7763021e34a1", null ],
    [ "User_Kp", "classTask__Controller_1_1Controller.html#a7fb1be0ab52694a6938e01c9d741ba62", null ]
];