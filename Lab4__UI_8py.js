var Lab4__UI_8py =
[
    [ "data_CSV", "Lab4__UI_8py.html#a0f9e76fea859fa55bb520c05d6bffaa2", null ],
    [ "data_list", "Lab4__UI_8py.html#a076a9cd7edbbf803952506277a6bee9c", null ],
    [ "data_string", "Lab4__UI_8py.html#a9bf821ace53d9229a0e22913d0a5892a", null ],
    [ "data_tup", "Lab4__UI_8py.html#ad1f889f1cb9b5c8ebd011c00da5fb7e5", null ],
    [ "delimiter", "Lab4__UI_8py.html#a5242776b489eaa2a98611101b91bf586", null ],
    [ "fmt", "Lab4__UI_8py.html#aa7d03142f9fa314c15f848fe8dd9f801", null ],
    [ "i", "Lab4__UI_8py.html#a2bf40eab8a4fa18d1b970a0b8aad0317", null ],
    [ "idx", "Lab4__UI_8py.html#ae6e0faa8376c1e1732539e0ed7548d29", null ],
    [ "input_cmd", "Lab4__UI_8py.html#a3029cfc484f0f14db861419b890f55df", null ],
    [ "multiple", "Lab4__UI_8py.html#a7c984744060012f841a29af3cbaea079", null ],
    [ "n", "Lab4__UI_8py.html#ad330e252a8cf76939f2fcc7060e31e68", null ],
    [ "position", "Lab4__UI_8py.html#a3c91773b4ee050ef9ce11d5b07dd779e", null ],
    [ "ser", "Lab4__UI_8py.html#a1869bcc5f86b9f2f62a157c6fb1be793", null ],
    [ "t", "Lab4__UI_8py.html#a242071e13a9cdec9c99ae9d362ac1f6c", null ],
    [ "t_end", "Lab4__UI_8py.html#a998d3c51fd00d92e749d218eeec7d153", null ],
    [ "t_start", "Lab4__UI_8py.html#adb7a0d4d2e29f584652953bb710be0f1", null ],
    [ "update_signal", "Lab4__UI_8py.html#a0c443114e1883beebcec46a1b6398e7d", null ]
];