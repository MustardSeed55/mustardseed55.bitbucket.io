var Lab6__UI_8py =
[
    [ "data_CSV", "Lab6__UI_8py.html#a0c86ab73e36281941b181d496b5cf720", null ],
    [ "data_list", "Lab6__UI_8py.html#a115801e520dd373be4f98821fd792f3c", null ],
    [ "data_string", "Lab6__UI_8py.html#a2fa014f2080fe29eb9f898be1b94be14", null ],
    [ "data_strip", "Lab6__UI_8py.html#a62de37720a64d9948a801f08341d9911", null ],
    [ "data_tup", "Lab6__UI_8py.html#adbabb77a960b9527bfa96e0e936f95f1", null ],
    [ "dbg", "Lab6__UI_8py.html#a493323885ce8346ded4f341e360ca3c6", null ],
    [ "delimiter", "Lab6__UI_8py.html#aee83c205eec242a0a17312a392d9e4ec", null ],
    [ "fmt", "Lab6__UI_8py.html#a556a6c77108d148c68e2cbf1e307ac50", null ],
    [ "i", "Lab6__UI_8py.html#aea7c54bc609388bddab3733bf0aa114f", null ],
    [ "input_cmd", "Lab6__UI_8py.html#a985c42e0a5ef7c259d8520a7bb31ba23", null ],
    [ "Kp", "Lab6__UI_8py.html#a73e2abecf79af57ac5e101838b4587c2", null ],
    [ "loc", "Lab6__UI_8py.html#af470643f9fd59311a91efabb55474235", null ],
    [ "n", "Lab6__UI_8py.html#a669e84baaa1687f2253a721d9dcd2b51", null ],
    [ "Omega", "Lab6__UI_8py.html#ada313f163b3e70ab2f43e5f4da77e84a", null ],
    [ "Omega_ref", "Lab6__UI_8py.html#a4f2328985bfa5430da84a48fadea8041", null ],
    [ "ser", "Lab6__UI_8py.html#a9912527a2bddd2214242bd556f31cadf", null ],
    [ "t", "Lab6__UI_8py.html#ad6228c10d9477da4e9f0f28caad87a81", null ],
    [ "val", "Lab6__UI_8py.html#ac15c895aeb025b05138870fcc25f8163", null ]
];