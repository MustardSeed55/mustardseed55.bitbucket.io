var classLab7__Task__Controller_1_1Motor =
[
    [ "__init__", "classLab7__Task__Controller_1_1Motor.html#a643ba70c3e6d9936ed8b10cf9c651fa5", null ],
    [ "disable", "classLab7__Task__Controller_1_1Motor.html#a25033980c01707fe019ea52ffe2a8a78", null ],
    [ "enable", "classLab7__Task__Controller_1_1Motor.html#a14eeedae205d81b9605468e4523a5ab0", null ],
    [ "set_duty", "classLab7__Task__Controller_1_1Motor.html#ac509300ac5a4387a52eb06771cfeb6ea", null ],
    [ "dbg", "classLab7__Task__Controller_1_1Motor.html#a4c65dfe0c0ca4d4cbddb4abff221a8c5", null ],
    [ "IN1", "classLab7__Task__Controller_1_1Motor.html#aae1689f4b68269b8838b3486184e420a", null ],
    [ "IN2", "classLab7__Task__Controller_1_1Motor.html#a4786db6d06bada747e57de1ace3cdc06", null ],
    [ "pin15", "classLab7__Task__Controller_1_1Motor.html#abb1248cbfe8d88f9ffd01078f1531a62", null ],
    [ "timer", "classLab7__Task__Controller_1_1Motor.html#aaaea346eff9cf703c88b72b1e8d67963", null ],
    [ "timer_ch_IN1", "classLab7__Task__Controller_1_1Motor.html#a5e20359604e443687a0c870776c6f629", null ],
    [ "timer_ch_IN2", "classLab7__Task__Controller_1_1Motor.html#a1a534cf7e6c00d09fef3edd62f524034", null ]
];