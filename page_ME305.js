var page_ME305 =
[
    [ "Lab 1: Fibonacci Sequence", "subpage_lab1.html", null ],
    [ "Elevator Finite-State-Machine", "subpage_Elevator.html", null ],
    [ "Lab 2: Cooperative LED Tasks", "subpage_lab2.html", null ],
    [ "Lab 3: Encoder Interaction", "subpage_lab3.html", null ],
    [ "Lab 4: Get Encoder Position Through UI & Use Data", "subpage_lab4.html", null ],
    [ "Lab 5: Control LED Through Mobile App", "subpage_lab5.html", null ],
    [ "Lab 6: Motor Control", "subpage_lab6.html", null ],
    [ "Lab 7: Motor Control (Reference Tracking)", "subpage_lab7.html", null ]
];