var classmpc9808_1_1MPC9808__I2C =
[
    [ "__init__", "classmpc9808_1_1MPC9808__I2C.html#a42fedb7975a8e5ffd76f33e10480ac51", null ],
    [ "celsius", "classmpc9808_1_1MPC9808__I2C.html#ad654c242639b9f90fc19c50a6a3c7f71", null ],
    [ "check", "classmpc9808_1_1MPC9808__I2C.html#af4c0111101a9a3805c3e90f88c931ff4", null ],
    [ "fahrenheit", "classmpc9808_1_1MPC9808__I2C.html#a7c1aec6f62e38d98fd0a7de3f0d8e0a0", null ],
    [ "Manufacturer_ID", "classmpc9808_1_1MPC9808__I2C.html#aa0722d527de19a8c6c42638258d4986b", null ],
    [ "Manufacturer_Memaddr", "classmpc9808_1_1MPC9808__I2C.html#acb5150e80c6604267c694aebf98f2c14", null ],
    [ "Nucleo_i2c", "classmpc9808_1_1MPC9808__I2C.html#a556a60f1fc5a3c362c1d9f8999c00e59", null ],
    [ "Read_Memaddr", "classmpc9808_1_1MPC9808__I2C.html#a38049085e28ac65bddd4cb15817de262", null ],
    [ "Slave_Addr", "classmpc9808_1_1MPC9808__I2C.html#a85ac37f5762163a892fad6de08e08dcd", null ]
];