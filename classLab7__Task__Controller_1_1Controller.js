var classLab7__Task__Controller_1_1Controller =
[
    [ "__init__", "classLab7__Task__Controller_1_1Controller.html#a7b2206b9f3dcb4ec259cb7f907588add", null ],
    [ "run", "classLab7__Task__Controller_1_1Controller.html#af50161ace252d49a2a7628a02f6c8394", null ],
    [ "transitionTo", "classLab7__Task__Controller_1_1Controller.html#ac6862fefac15f72e14289390013e9c47", null ],
    [ "ClosedLoop", "classLab7__Task__Controller_1_1Controller.html#a0d456f6689a56728e3a313be213f7644", null ],
    [ "current_time", "classLab7__Task__Controller_1_1Controller.html#a3d400a7078fda14eda3dcefe8c5be20f", null ],
    [ "dbg", "classLab7__Task__Controller_1_1Controller.html#a14026cc285097c67d358b493416f1558", null ],
    [ "Encoder", "classLab7__Task__Controller_1_1Controller.html#ad09dbf766447f86d242178db86826238", null ],
    [ "i", "classLab7__Task__Controller_1_1Controller.html#ab3a3e6af5a5017ffae979c4f95abf18d", null ],
    [ "Motor", "classLab7__Task__Controller_1_1Controller.html#aa13209de5113e2b5d20e775d1a8d3441", null ],
    [ "next_time", "classLab7__Task__Controller_1_1Controller.html#a56274a731ddfbd0fe837713a022d23a7", null ],
    [ "Omega", "classLab7__Task__Controller_1_1Controller.html#ac6501ef8fd628783b270dafc8ec58eff", null ],
    [ "Omega_ref", "classLab7__Task__Controller_1_1Controller.html#ade9c584c34aaf555db667ad4d6c0af8e", null ],
    [ "period", "classLab7__Task__Controller_1_1Controller.html#aa838bb8d07e3cf3430261ca9999987fa", null ],
    [ "run_time", "classLab7__Task__Controller_1_1Controller.html#a3285fe18389d14067950324bcb8520a1", null ],
    [ "runs", "classLab7__Task__Controller_1_1Controller.html#a6488881b6cb272e064572ca3e8f88dc7", null ],
    [ "ser", "classLab7__Task__Controller_1_1Controller.html#a3e747c0edfc1cd667b221b1bd8673b24", null ],
    [ "start_time", "classLab7__Task__Controller_1_1Controller.html#afa9eb101d9057917aaaef50bd499aafb", null ],
    [ "state", "classLab7__Task__Controller_1_1Controller.html#ade665d1c6ab48487ed5b4a478a32e08e", null ],
    [ "theta", "classLab7__Task__Controller_1_1Controller.html#ae68fcbcef04c9679dc218fb2a96eb874", null ],
    [ "time", "classLab7__Task__Controller_1_1Controller.html#a93f4fa4cd9d4d83079d9477a59e2b7e3", null ],
    [ "User_Kp", "classLab7__Task__Controller_1_1Controller.html#a705c243fd5d8d4509b825150bc1fc5b0", null ]
];