var classME405__Touchscreen_1_1TouchScreen =
[
    [ "__init__", "classME405__Touchscreen_1_1TouchScreen.html#a9cd2e1a2d24d85f68d4368e437c00c1a", null ],
    [ "Optimized_Scan_Y", "classME405__Touchscreen_1_1TouchScreen.html#a070bcccefe0de24244ebbf1e378873bd", null ],
    [ "Scan_X", "classME405__Touchscreen_1_1TouchScreen.html#a6bd6b13fcc41c107292eb4cbf37c3363", null ],
    [ "Scan_XYZ", "classME405__Touchscreen_1_1TouchScreen.html#a2b3cc639409f36c5c5af7ba8e8f5d069", null ],
    [ "Scan_Y", "classME405__Touchscreen_1_1TouchScreen.html#a33391b351bd39e153df35045274ac880", null ],
    [ "Scan_Z", "classME405__Touchscreen_1_1TouchScreen.html#a17b7dc05f069f87549b7fe7ec4bd5140", null ],
    [ "pin_xm", "classME405__Touchscreen_1_1TouchScreen.html#a487a10d05ae25c6702cf72f4bf880872", null ],
    [ "pin_xp", "classME405__Touchscreen_1_1TouchScreen.html#ac97f437a47a5951ea312c89b27cc65f5", null ],
    [ "pin_ym", "classME405__Touchscreen_1_1TouchScreen.html#af70013b306c8e20ea90d3387c7d9a757", null ],
    [ "pin_yp", "classME405__Touchscreen_1_1TouchScreen.html#af71e7a80815518f5e00235cff1f845fa", null ],
    [ "scan_tup", "classME405__Touchscreen_1_1TouchScreen.html#a5ed3030ba08ae59d06154f12eab9cade", null ],
    [ "X_convert", "classME405__Touchscreen_1_1TouchScreen.html#a97835adca4a157f9aad1a980e8935a8a", null ],
    [ "xc", "classME405__Touchscreen_1_1TouchScreen.html#a375ddf81ff97fc2565303b22baf36f56", null ],
    [ "Y_convert", "classME405__Touchscreen_1_1TouchScreen.html#a6df63d75fb04538d6fee4476e9cd5a05", null ],
    [ "yc", "classME405__Touchscreen_1_1TouchScreen.html#ade05509b9933a2e062e74ccdd04282b2", null ]
];